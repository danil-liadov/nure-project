from django.urls import path

import game.views


urlpatterns = [
    path('menu/', game.views.main, name='game_menu'),
    path('start/', game.views.start, name='game_start'),
]
