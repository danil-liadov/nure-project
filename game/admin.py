from django.contrib import admin
from .models import UserHighScore, GamersCount

admin.site.register(UserHighScore)
admin.site.register(GamersCount)
# Register your models here.
