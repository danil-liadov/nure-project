from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import UserHighScore
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


@login_required
def main(request):
    queryset = UserHighScore.objects.order_by('-score')[:5]
    to_retrieve = UserHighScore.objects.filter(user=request.user).first()
    logged_as = request.user.username
    registered = User.objects.count()
    if to_retrieve is not None:
        high_score = to_retrieve.score
        if high_score == 0:
            high_score = None
    else:
        high_score = None

    return render(request, 'game/menu.html', context={'high_score': high_score,
                                                      'registered': registered,
                                                      'logged_as': logged_as,
                                                      'tops': queryset,
                                                      })


@login_required
def start(request):
    hero_name = request.user.first_name
    to_update = UserHighScore.objects.filter(user=request.user).first()
    if not to_update:
         UserHighScore.objects.create(user=request.user)
    else:
        if request.method == 'POST' and request.is_ajax:
            to_retrieve = dict(request.POST)
            score = int(to_retrieve.get('score')[0])
            if to_update.score < score:
                to_update.score = score
                to_update.save()
    return render(request, 'game/startgame.html', context={'name': hero_name})


def redirector(request):
    return HttpResponseRedirect('game/menu')
