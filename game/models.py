from django.db import models
from django.contrib.auth.models import User


class UserHighScore(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)


class GamersCount(models.Model):
    @property
    def counter(self):
        User.objects.count()
