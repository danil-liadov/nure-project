""" Tasks URL Configuration
"""
from django.urls import path

import users.views


urlpatterns = [
    path('login/', users.views.login, name='user_login'),
    path('logout/', users.views.logout, name='user_logout'),
    path('register/', users.views.register, name='user_register'),
]
