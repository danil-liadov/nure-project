"""
    Login, Logout and Register functionality views.
"""
from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login, logout as django_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.urls import reverse

from users.forms import AuthorizeForm, RegisterForm


def login(request):
    """[View] Authorize to Example Project.
    """
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('game_menu'))
    if request.method != 'POST':
        form = AuthorizeForm()
    else:
        form = AuthorizeForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            user = authenticate(
                username=form_data['username'], password=form_data['password'])
            if user:
                django_login(request, user)
                if form_data['redirect']:
                    return HttpResponseRedirect(form_data['redirect'])
                else:
                    return HttpResponseRedirect(reverse('game_menu'))

    return render(request,
                  'users/login.html',
                  {'form': form.as_p, 'error': form.non_field_errors, 'request': request})


@login_required
def logout(request):
    """[View] De-authorize from Example Project.
    """
    django_logout(request)
    return HttpResponseRedirect(reverse('user_login'))


def register(request):
    """[View] Register new user.
    """
    if request.method != 'POST':
        form = RegisterForm()
    else:
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(**form.cleaned_data)
            user.save()
            django_login(request, user)
            return HttpResponseRedirect(reverse('game_menu'))

    return render(request, 'users/register.html',
                  {'form': form.as_p, 'error': form.non_field_errors, 'request': request})
