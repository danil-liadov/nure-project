from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth import authenticate


class AuthorizeForm(forms.Form):
    """Form for  authorization.
    """
    username = forms.EmailField(label='Login', max_length=50, min_length=3)
    password = forms.CharField(label='Password', max_length=50, min_length=3)
    redirect = forms.CharField(required=False)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if user is None:
            raise forms.ValidationError("Such password and login pair doesn't exist")
        else:
            pass


class RegisterForm(forms.Form):
    """Form for new user registering.
    """
    username = forms.EmailField(label='Login', max_length=50, min_length=6)
    password = forms.CharField(label='Password', max_length=50, min_length=6)
    first_name = forms.CharField(label='First name', max_length=50,
                                 required=False, help_text='Optional.')

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError("Email already exists")
        return username


