# How to deploy this project?

1. Pull the latest version of project from this repository via git.
2. Enter the project directory via cli (should be like that (windows example)):

```
C:\...dirs here...\<project directory> >
```
3. Launch Docker
4. Enter this command via cli: `docker-compose up --build`
5. Project will be builded and launched. After that enter `127.0.0.1` in browser and you will be redirectetd to login page.
6. Click register new user, add credentials and you are ready to play the game!