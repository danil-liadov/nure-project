function CssErrorDisplay(idToEdit,errorDisplay,StringToDisplay){
   $(idToEdit).css("border-color","red");
   $(errorDisplay).text(StringToDisplay);
}
$(".valid_auth_forms").click(function(event){
    event.preventDefault();
});
function ValidateForms(){
    if($("#id_login").val()==""){
      CssErrorDisplay("#id_login",".error_display","Empty login form!")
    }
    else if($("#id_login").val().length > 50){
    CssErrorDisplay("#id_login",".error_display","Too long login form!")
    }
    else if($("#id_login").val().length < 5){
    CssErrorDisplay("#id_login",".error_display","Too short login form!")
    }
    else if ($("#id_password").val()=="") {
      $("#id_login").css("border-color","green")
      CssErrorDisplay("#id_login",".error_display","Empty password form!")
    }
    else if ($("#id_password").val().length < 5) {
      $("#id_login").css("border-color","green")
      CssErrorDisplay("#id_login",".error_display","Too short password form!")
    }
    else if ($("#id_password").val().length > 50) {
      $("#id_login").css("border-color","green")
      CssErrorDisplay("#id_login",".error_display","Too long password form!")
    }
    else {
      $("#id_login").css("border-color","green")
      $("#id_password").css("border-color","green")
      $(".error_display").text("")
      $(".valid_auth_forms").unbind("click")
    }
}