//utility for game over bar
var wid = window.innerWidth;
var hei = window.innerHeight;
$("footer").css("width",wid);
$("footer").css("height",hei);
//utility for getting csrf tokens and sending post requests
function getCookie(name) {
          var cookieValue = null;
          if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
               var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
             }
          }
      }
 return cookieValue;
}
function scorePost(value){
$.ajax({
       url : window.location.href, // the endpoint,commonly same url
       type : "POST", // http method
       data : { csrfmiddlewaretoken : getCookie('csrftoken'), 
       score : value,
 }, // data sent with the post request
 });
}
//Fixed stack in js
function Stack(length, array) {
    if(array.length > length){
        array.pop()
    }
    return array
}
// Startgame parameters
    var Heal_Amount = 0;
    var turns = 0;
    var battleHunger = 0;
    var char = {
        name:"",
        hitp:100,
        attack:1,
        inventory: []
    }
    var enemy={
        name:"Start Enemy",
        type:"standart_enemy",
        hitp:getRandomInt(10,20),
        attack:getRandomInt(2,5)
    }
    char.name=$('sub_name').innerHtml;
    char.hitp=100;
    char.attack=1;
    ShowValues();
    $(".status_bar").text("You have started the game.Press 'Take your turn' button to take the turn.")
//Game processes and functionsc
    function HealPotionFind(a){
        var b = getRandomInt(1,30)
        $(".heal_counter").text(Heal_Amount);
            if (a<15) {
                if (b>=15) {
                    $(".status_bar").text("You have found heal potion!")
                    Heal_Amount++;
                    StatusExcept("StatusFindP");
                }
            }
        }
    function HealPotionUse(){
        if (Heal_Amount<=0) {
            alert("You don't have potions!!")
            return false;
        }
        else{
        char.hitp = char.hitp + getRandomInt(10,30+turns*2);
        Heal_Amount--;
        ShowValues();
        $(".heal_counter").text(Heal_Amount);
        }
    }
    function NextTurn(){
        var check = getRandomInt(10,20);
        char.inventory.unshift(GenerateItemProperties(turns))
        char.inventory = Stack(5, char.inventory)
        console.log(char.inventory);
        turns++;
        scorePost(turns);
        battleHunger++;
        if(turns%20==0){
        	$(".status_bar").text("You have met a Boss!");
            GenerateDaBoss();
            ShowEnemyValues();
            $(".output_enemy").css("visibility","visible");
            $(".at_enemy_button").css("visibility","visible");
        }
        else{
        if(battleHunger <=20){
        if (check>=15) {
            $(".output_enemy").css("visibility","visible")
            $(".status_bar").text("You have met an enemy");
            GenerateEnemy();
            ShowEnemyValues();
            $(".at_enemy_button").css("visibility","visible");
            ShowValues();
            StatusExcept("StatusFindE");
        }
        else{
            $(".status_bar").text("You haven't met anyone.Your hp and attack increased");
            $(".output_enemy").css("visibility","hidden")
            char.hitp+=3;
            char.attack+=1;
            $(".at_enemy_button").css("visibility","hidden");
            StatusExcept("StatusRest");
            ShowValues();
        }
                HealPotionFind(check);
    }
    else{
        EndGameShow(0);
    }
}
        return check;
    }
    function AttackEnemy(){
        char.hitp=char.hitp-enemy.attack;
        enemy.hitp =enemy.hitp - char.attack;
        battleHunger = 0;
        ShowEnemyValues()
        if (char.hitp <=0) {
        	EndGameShow(1);
            $("footer").css("visibility","visible")
        }
        if (enemy.hitp<=0) {
            char.hitp+=10;
            char.attack+=2;
            $(".output_enemy").css("visibility","hidden");
            $(".at_enemy_button").css("visibility","hidden");
            $(".status_bar").text("You killed an enemy.Your attack and hp increased.");
            battleHunger=0;
        }
        StatusExcept("StatusAttack");
        ShowValues();
        var leftToDeath=20-battleHunger;
        $(".Hunger").text(leftToDeath);
    }
    function GenerateTreasureName(step){
        var NameArr = ["","Da boss shoota","Health-powered ring","Attack-powered ring","Damned Anihilator","Crappy damage blocker"]
        var temp = getRandomInt(1,6);
        for (var i = 0; i < NameArr.length; i++) {
            if (i == temp) {
                ItemName = NameArr[i];
                return ItemName;
                break
            }
            else{
                continue
            }
        } 
    }
    function GenerateItemProperties(step){
        var inventoryItem = {
            name:'',
            type_of_buff:'',
            buff_against:'',
            points_of_buff: 0,
            is_usable: false,
            one_use_only: false,
            description: '',
            imageUrl: '',    
        }
        item = inventoryItem
        item.name = GenerateTreasureName(step)
        if(step === 0){
            step = 1
        } else {
            step = step
        }
        if(item.name==="Da boss shoota"){
            item.type_of_buff='kill'
            item.description='This imbalanced weapon can help you by killing one boss and after that it will dissapear from your inventory';
            item.buff_against='boss';
            item.points_of_buff = step*10000000;
            item.is_usable = true;
            item.one_use_only = true;
            item.imageUrl = ''; /* TODO: Generate item image url */
        } else if (item.name==="Health-powered ring"){
            item.type_of_buff='heal';
            item.description='Well, this is ring which can heal your hp with each step. Passive healing with no usage';
            item.buff_against='none';
            item.points_of_buff =Math.round(step*0.55);
            item.is_usable = false;
            item.one_use_only = false;
        } else if (item.name==="Attack-powered ring"){
            item.type_of_buff='attack';
            item.description='Well, this is ring which can increase your attack permanently. Passive attack increment with no usage';
            item.buff_against='none';
            item.points_of_buff =Math.round(step*0.6);
            item.is_usable = false;
            item.one_use_only = false;
        } else if (item.name==="Damned Anihilator"){
            item.type_of_buff='kill'
            item.description='Oh my God, it is f*****g Anihilator! Kills your enemy(not boss) permanently without any obstacles'
            item.buff_against='standart_enemy';
            item.points_of_buff = step*10000000;
            item.is_usable = true;
            item.one_use_only = true
        } else if (item.name==="Crappy damage blocker") {
            item.type_of_buff='block';
            item.description='Hmm, sounds not good as all items in this game, but it is useful, because it can block enemy points of attack';
            item.buff_against='all';
            item.points_of_buff = Math.round(step*0.3);
            item.is_usable = true;
            item.one_use_only = true
        }
        item.name = item.name +' lvl '+ step;
        return item
    }
//Enemy generation
    function GenerateDaBoss(){
    var hpenemy=30+turns*3;
    var atenemy=0+turns;
    enemy.name=GenerateBossName();
    enemy.type="boss",
    enemy.hitp = hpenemy;
    enemy.attack = atenemy;
    ShowEnemyValues();
    }
    function GenerateEnemyName(){
        var NameArr=["","Ogre","Goblin","Ork","Dwarf"];
        var EnemyName="";
        var temp = getRandomInt(1,4);
        for (var i = 0; i < NameArr.length; i++) {
            if (i = temp) {
                Enemyname = NameArr[i];
                return Enemyname;
            }
            else{
                continue
            }
        }
    }
    function GenerateEnemy(){
    var hpenemy=getRandomInt(10,20);
    var atenemy=getRandomInt(1,3);
    enemy.name=GenerateEnemyName();
    enemy.type="standart_enemy";
    enemy.hitp = hpenemy+turns;
    enemy.attack = atenemy+turns;
    ShowEnemyValues();
    }
        function GenerateBossName(){
        var NameArr=["","Big Deal Man","Sneaky peaky corpse","Curios small spider","Nothing to do with it but it is boss!"];
        var EnemyName="";
        var temp = getRandomInt(1,4);
        for (var i = 0; i < NameArr.length; i++) {
            if (i = temp) {
                Enemyname = NameArr[i];
                return Enemyname;
            }
            else{
                continue
            }
        }
    }
//Functions of output 
    function ShowValues(){ 
    var leftToDeath=20-battleHunger;  
    $(".outname").text("Your name is ");
    $(".sub_name").text(char.name);
    $(".outhp").text("Your hp is ",);
    $(".sub_hp").text(char.hitp);
    $(".outat").text("Your attack is ",);
    $(".sub_outat").text(char.attack);
    $(".turns_taken").text(turns);
    $(".Hunger").text(leftToDeath);
    }
    function ShowEnemyValues(){
    $(".outname_en").text("Enemy name is ");
    $(".sub_name_en").text(enemy.name);
    $(".outhp_en").text("Enemy hp is ");
    $(".sub_hp_en").text(enemy.hitp);
    $(".outat_en").text("Enemy attack is ");
    $(".sub_at_en").text(enemy.attack);
    $(".turns_taken").text(turns);
    }
//Utilities
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    function StatusExcept(string){
    	var arr=["StatusAttack","StatusFindP","StatusRest","StatusFindE"]
    	for (var i = 0; i < arr.length; i++) {
    		if (arr[i]==string) {
    			$(".imageStatus").addClass(arr[i]);
    		}
    		else{
    			$(".imageStatus").removeClass(arr[i]);
    		}	
    	}
    }
    function EndGameShow(int){
    	var prop_Count= "Your score is " + turns;
    	$("footer").css("visibility","visible")
    	if (int != 0) {
    		var stringReason="You have been killed by "+enemy.name;
    		$(".deathReason").text(stringReason);
    	}
    	else{
    		$(".deathReason").text("You haven't satisfied your battle hunger!")
    	}
    	$(".endScore").text(prop_Count);
    }